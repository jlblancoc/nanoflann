Source: nanoflann
Section: libs
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Timo Röhling <timo@gaussglocke.de>, José Luis Blanco Claraco <joseluisblancoc@gmail.com>
Build-Depends: debhelper-compat (= 13), cmake, libgtest-dev <!nocheck>, doxygen <!nodoc>
Homepage: https://github.com/jlblancoc/nanoflann
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/science-team/nanoflann.git
Vcs-Browser: https://salsa.debian.org/science-team/nanoflann

Package: libnanoflann-dev
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: C++11 header-only library for Nearest Neighbor Search with KD-Trees
 Nanoflann is a fork of the FLANN library. Originally born as a child project
 of the Mobile Robot Programming Toolkit, nanoflann is now available as
 stand-alone library.
 .
 It trades some of the flexibility of FLANN for raw execution speed and is much
 more memory efficient, as it avoids unnecessary copies of point cloud data.
